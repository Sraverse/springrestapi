package com.cognizant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.dao.CourseDTO;
import com.cognizant.dao.StudentDTO;
import com.cognizant.exceptions.BadRequestException;
import com.cognizant.exceptions.ErrorDetails;
import com.cognizant.exceptions.ResourceAlreadyExists;
import com.cognizant.exceptions.ResourceNotFoundException;
import com.cognizant.models.Student;
import com.cognizant.services.StudentServices;

@RestController
@RequestMapping("/api")
public class StudentController {

	@Autowired
	private StudentServices studentservices;

	@GetMapping(value = "/student")
	public ResponseEntity<CourseDTO> getAllCourses() {
		CourseDTO coursedto = new CourseDTO();
		coursedto.setCourselist(studentservices.getAllCourses());

		return new ResponseEntity<CourseDTO>(coursedto, HttpStatus.OK);
	}

	@PostMapping(value = "/student")
	public ResponseEntity<?> postStudent(@RequestBody Student student) {
		System.out.println(student);
		Student st = studentservices.getStudentById(student.getEnrollmentId());
//		return student;
		try {
			if (st == null) {
				studentservices.postStudent(student);
				return new ResponseEntity<Student>(student, HttpStatus.CREATED);
			} else {
				ResourceAlreadyExists rnfe = new ResourceAlreadyExists("Student Already Exists");
				System.out.println(rnfe.getMessage());
				ErrorDetails errors = new ErrorDetails(rnfe.getMessage());
				return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);

		}
//		
	}

	@DeleteMapping(value = "/Student/{enrollmentId}")
	public ResponseEntity<?> deleteStudent(@PathVariable("enrollmentId") int EnrollmentId) {
//		System.out.println(StudentId);
		boolean res = studentservices.deleteStudent(EnrollmentId);
		try {
			if (res == true) {
				return new ResponseEntity<String>("Deletion Successful", HttpStatus.OK);
			} else {
				ResourceNotFoundException rnfe = new ResourceNotFoundException("No Enrollment Information Available");
				System.out.println(rnfe.getMessage());
				ErrorDetails errors = new ErrorDetails(rnfe.getMessage());
				return new ResponseEntity<Object>(errors, HttpStatus.NOT_FOUND);
//				return new ResponseEntity<ResourceNotFoundException>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);

		}

	}

}
